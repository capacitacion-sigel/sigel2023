package clasesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import clasesPrincipales.CP01_Validacion_Menu_Principal;
import clasesPrincipales.CP02_Agregar_producto_al_carrito;
import clasesPrincipales.conexionChrome;

import org.testng.annotations.BeforeMethod;

public class TestValidacionMenuPrincipal {
conexionChrome cd=new conexionChrome();
WebDriver wd;

@BeforeMethod
public void setUp() {
  wd=cd.ConexionChrome();
}

@Test(priority = 0)
public void CP01_Validacion_Menu_Principal() {
	CP01_Validacion_Menu_Principal mp=new CP01_Validacion_Menu_Principal(wd);
    mp.ValidacionMenuPrincipal();
}
@Test(priority = 1)
public void CP02_Agregar_producto_al_carrito_() {
	CP02_Agregar_producto_al_carrito ac=new CP02_Agregar_producto_al_carrito(wd);
ac.CP02_Agregar_producto_al_carrito();
}

@AfterMethod
public void tearDown() {
	wd.quit();
}

}
